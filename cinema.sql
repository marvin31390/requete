-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Mar 17 Novembre 2020 à 23:33
-- Version du serveur :  5.7.32-0ubuntu0.18.04.1
-- Version de PHP :  7.2.24-0ubuntu0.18.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `cinema`
--

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `idclient` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `prénom` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`idclient`, `nom`, `prénom`, `email`) VALUES
(1, 'pierre', 'jean', 'jean-pierre@gmail.com'),
(2, 'romain', 'gaulle', 'romain-gaulle@gmail.com'),
(3, 'christophe', 'chinois', 'christophe-chinois@gmail.com'),
(4, 'marvin', 'diou', 'marvin-diou@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `film`
--

CREATE TABLE `film` (
  `idfilm` int(11) NOT NULL,
  `titre` varchar(100) NOT NULL,
  `durée` varchar(6) NOT NULL,
  `genre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `film`
--

INSERT INTO `film` (`idfilm`, `titre`, `durée`, `genre`) VALUES
(1, 'hot-shot', '84', 'comedie-guerre'),
(2, 'hot-shot 2', '89', 'comedie-guerre'),
(3, 'scary-movie', '90', 'comedie-horreur'),
(4, 'scary-movie 2', '89', 'comedie-horreur'),
(5, 'scary-movie 3', '85', 'comedie-horreur');

-- --------------------------------------------------------

--
-- Structure de la table `moyen`
--

CREATE TABLE `moyen` (
  `idmoyen` int(11) NOT NULL,
  `idpaiement` int(11) NOT NULL,
  `paypal` varchar(50) NOT NULL,
  `cb` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `paiement`
--

CREATE TABLE `paiement` (
  `idpaiement` int(11) NOT NULL,
  `idréservation` int(11) NOT NULL,
  `méthode` varchar(50) NOT NULL,
  `moyen` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `réservation`
--

CREATE TABLE `réservation` (
  `idréservation` int(11) NOT NULL,
  `idclient` int(11) NOT NULL,
  `idfilm` int(11) NOT NULL,
  `idséance` int(11) NOT NULL,
  `tarif` float NOT NULL,
  `quantité` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `réservation`
--

INSERT INTO `réservation` (`idréservation`, `idclient`, `idfilm`, `idséance`, `tarif`, `quantité`) VALUES
(33, 1, 2, 1, 3.5, 2),
(34, 2, 1, 2, 3.5, 4),
(35, 3, 3, 3, 3.5, 1),
(36, 4, 4, 4, 3.5, 2),
(37, 5, 5, 5, 3.5, 3);

-- --------------------------------------------------------

--
-- Structure de la table `salles`
--

CREATE TABLE `salles` (
  `idsalles` int(11) NOT NULL,
  `idfilm` int(11) NOT NULL,
  `nb_places` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `salles`
--

INSERT INTO `salles` (`idsalles`, `idfilm`, `nb_places`) VALUES
(1, 2, 50),
(2, 1, 50),
(3, 3, 50),
(4, 4, 50),
(5, 5, 50);

-- --------------------------------------------------------

--
-- Structure de la table `séance`
--

CREATE TABLE `séance` (
  `idséance` int(11) NOT NULL,
  `idsalles` int(11) NOT NULL,
  `idfilm` int(11) NOT NULL,
  `date_seance` date NOT NULL,
  `heure_seance` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `séance`
--

INSERT INTO `séance` (`idséance`, `idsalles`, `idfilm`, `date_seance`, `heure_seance`) VALUES
(1, 1, 2, '2020-11-17', '17:30:00'),
(2, 2, 1, '2020-11-17', '18:00:00'),
(3, 3, 3, '2020-11-17', '18:30:00'),
(4, 4, 4, '2020-11-17', '19:00:00'),
(5, 5, 5, '2020-11-17', '19:30:00');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`idclient`);

--
-- Index pour la table `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`idfilm`);

--
-- Index pour la table `moyen`
--
ALTER TABLE `moyen`
  ADD PRIMARY KEY (`idmoyen`);

--
-- Index pour la table `paiement`
--
ALTER TABLE `paiement`
  ADD PRIMARY KEY (`idpaiement`),
  ADD KEY `paiement ibfk 1` (`idréservation`);

--
-- Index pour la table `réservation`
--
ALTER TABLE `réservation`
  ADD PRIMARY KEY (`idréservation`),
  ADD KEY `idfilm` (`idfilm`),
  ADD KEY `idséance` (`idséance`),
  ADD KEY `réservation_ibfk` (`idclient`);

--
-- Index pour la table `salles`
--
ALTER TABLE `salles`
  ADD PRIMARY KEY (`idsalles`),
  ADD KEY `salles ibfk 1` (`idfilm`);

--
-- Index pour la table `séance`
--
ALTER TABLE `séance`
  ADD PRIMARY KEY (`idséance`),
  ADD KEY `séance ibfk 1` (`idsalles`),
  ADD KEY `séance ibfk 2` (`idfilm`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `idclient` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `film`
--
ALTER TABLE `film`
  MODIFY `idfilm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `moyen`
--
ALTER TABLE `moyen`
  MODIFY `idmoyen` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `paiement`
--
ALTER TABLE `paiement`
  MODIFY `idpaiement` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `réservation`
--
ALTER TABLE `réservation`
  MODIFY `idréservation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT pour la table `salles`
--
ALTER TABLE `salles`
  MODIFY `idsalles` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `séance`
--
ALTER TABLE `séance`
  MODIFY `idséance` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `paiement`
--
ALTER TABLE `paiement`
  ADD CONSTRAINT `paiement ibfk 1` FOREIGN KEY (`idréservation`) REFERENCES `réservation` (`idréservation`);

--
-- Contraintes pour la table `réservation`
--
ALTER TABLE `réservation`
  ADD CONSTRAINT `réservation_ibfk` FOREIGN KEY (`idclient`) REFERENCES `client` (`idclient`),
  ADD CONSTRAINT `réservation_ibfk_3` FOREIGN KEY (`idséance`) REFERENCES `séance` (`idséance`);

--
-- Contraintes pour la table `salles`
--
ALTER TABLE `salles`
  ADD CONSTRAINT `salles ibfk 1` FOREIGN KEY (`idfilm`) REFERENCES `film` (`idfilm`);

--
-- Contraintes pour la table `séance`
--
ALTER TABLE `séance`
  ADD CONSTRAINT `séance ibfk 1` FOREIGN KEY (`idsalles`) REFERENCES `salles` (`idsalles`),
  ADD CONSTRAINT `séance ibfk 2` FOREIGN KEY (`idfilm`) REFERENCES `film` (`idfilm`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
